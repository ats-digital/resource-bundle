<?php

namespace ATS\CoreBundle\Tests\Controller;

use ATS\ResourceBundle\Document\Resource;
use ATS\ResourceBundle\Manager\ResourceManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileOperationsControllerTest extends WebTestCase
{
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->container = $kernel->getContainer();
        $this->resourceManager = $kernel->getContainer()->get(ResourceManager::class);
        $this->client = self::createClient([]);

        // Create sample file for upcoming tests
        $fs = new Filesystem();
        $this->sampleFile = $fs->tempnam(sys_get_temp_dir(), '__resource__');
        $fs->dumpFile($this->sampleFile, "42");
        $this->uploadedFile = new UploadedFile(
            $this->sampleFile,
            'meaning_of_life.txt',
            'text/plain'
        );
    }

    public function testFailIfEmptyUploadAction()
    {
        // Upload nothing and get F'd in the A

        $this->expectException(BadRequestHttpException::class);
        $this->client->request(
            'POST',
            '/api/resource',
            [],
            [],
            []
        );
    }

    public function testFailIfInvalidSubdirectoryAction()
    {
        // Upload to a restricted subdir and get F'd in the A

        $this->expectException(BadRequestHttpException::class);
        $this->client->request(
            'POST',
            '/api/resource',
            ['subdir' => 'qux'],
            ['file' => $this->uploadedFile],
            []
        );
    }

    public function testRootUploadAction()
    {
        $this->resourceManager->deleteAll();

        // root file upload

        $this->client->request(
            'POST',
            '/api/resource',
            [],
            ['file' => $this->uploadedFile],
            []
        );

        $response = $this->client->getResponse();
        $this->assertTrue($response->isSuccessful());

        $resourceId = json_decode($response->getContent())->id;
        $originalFileName = json_decode($response->getContent())->original_file_name;

        $resource = $this->resourceManager->getOneBy(['id' => $resourceId]);
        $this->assertNotNull($resource);
        $this->assertEquals($this->uploadedFile->getClientOriginalName(), $originalFileName);
        $this->assertTrue($resource->isValid());
    }

    public function testSubdirectoryUploadAction()
    {
        // valid subdir file upload

        $this->client->request(
            'POST',
            '/api/resource',
            ['subdir' => 'foo'],
            ['file' => $this->uploadedFile],
            []
        );
        $response = $this->client->getResponse();
        $this->assertTrue($response->isSuccessful());

        $resourceId = json_decode($response->getContent())->id;
        $resource = $this->resourceManager->getOneBy(['id' => $resourceId]);
        $this->assertNotNull($resource);
        $this->assertTrue($resource->isValid());
    }

    public function testDownloadAction()
    {

        $randomId = (new \MongoId())->__toString();
        $resource = $this->resourceManager->getOneBy([]);
        $this->assertNotNull($resource);

        $this->client->request(
            'GET',
            '/api/resource/' . $resource->getId()
        );

        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $this->client->request(
            'GET',
            '/api/resource/$randomId'
        );

        $this->assertEquals($this->client->getResponse()->getStatusCode(), 404);

        $resource->setPath('__invalid__');
        $this->resourceManager->update($resource);

        $this->client->request(
            'GET',
            '/api/resource/' . $resource->getId()
        );

        $this->assertEquals($this->client->getResponse()->getStatusCode(), 404);
    }
}
