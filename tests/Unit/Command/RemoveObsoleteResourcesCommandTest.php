<?php

namespace ATS\ResourceBundle\Tests\Unit\Command;

use ATS\ResourceBundle\Command\RemoveObsoleteResourcesCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

class RemoveObsoleteResourcesCommandTest extends KernelTestCase
{

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->documentManager = $kernel->getContainer()
        ->get('doctrine_mongodb')
        ->getManager();

        $this->uploadsDir = $kernel->getContainer()->getParameter("uploads_dir");
        $this->documentManager->getSchemaManager()->dropDatabases();
    }

    public function testRemoveObsoleteResourcesIfAny()
    {
        $kernel = self::bootKernel();

        (new Filesystem())->touch($this->uploadsDir . '/somewhere over the rainbow.txt');

        $application = new Application($kernel);

        $application->add(
            (new RemoveObsoleteResourcesCommand())
        );

        $command = $application->find('ats:resource:upload:remove-obsolete');
        $commandTester = new CommandTester($command);

        // Obsolete file exists, user cancels at prompt

        $commandTester->setInputs(['n']);
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
        $this->assertEquals($returnCode, RemoveObsoleteResourcesCommand::RETURN_CODE_NOOP_CANCELED);

        // Obsolete file exists, user proceeds with removal

        $commandTester->setInputs(['y']);
        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
        $this->assertEquals($returnCode, RemoveObsoleteResourcesCommand::RETURN_CODE_REMOVAL_OK);

        // Obsolete file(s) already removed, nothing to do

        $returnCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ],
            [
                'interactive' => false
            ]
        );
        $this->assertEquals($returnCode, RemoveObsoleteResourcesCommand::RETURN_CODE_NOOP_DEFAULT);
    }
}
