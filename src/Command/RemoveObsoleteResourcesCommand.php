<?php

namespace ATS\ResourceBundle\Command;

use ATS\ResourceBundle\Document\Resource;
use ATS\ResourceBundle\Manager\ResourceManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class RemoveObsoleteResourcesCommand extends ContainerAwareCommand
{

    const RETURN_CODE_NOOP_DEFAULT = 0;
    const RETURN_CODE_NOOP_CANCELED = -1;
    const RETURN_CODE_REMOVAL_OK = 1;
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ats:resource:upload:remove-obsolete')
            ->setDescription('Removes obsolete files from upload resources');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resourceManager = $this->getContainer()->get(ResourceManager::class);
        $uploadedResources = $resourceManager->getAll();
        $uploadedResourcePaths = array_map(
            function (Resource $resource) {
                return $resource->getPath();
            },
            $uploadedResources
        );

        $allResourcePaths = [];

        $finder = new Finder();
        $finder->files()->in($this->getContainer()->getParameter('uploads_dir'));
        foreach ($finder as $file) {
            $allResourcePaths[] = $file->getRealPath();
        }

        $obsoletePaths = array_diff($allResourcePaths, $uploadedResourcePaths);

        if (empty($obsoletePaths) === false) {
            $output->writeln(
                "<comment> The following files will be removed : "
                . PHP_EOL
                . join(PHP_EOL, $obsoletePaths) . "</comment>"
            );

            $questionHelper = $this->getHelper('question');
            $confirmationQuestion = new ConfirmationQuestion('<question>Do you confirm removal ?</question>', false);
            if ($questionHelper->ask($input, $output, $confirmationQuestion) === true) {
                (new Filesystem())->remove($obsoletePaths);
                $output->writeln("<info>All obsolete files removed</info>");
                return self::RETURN_CODE_REMOVAL_OK;
            }
            $output->writeln("<info>Aborting removal (by user cancellation)</info>");
            return self::RETURN_CODE_NOOP_CANCELED;
        }
        $output->writeln("<info>No obsolete files found, exiting.</info>");
        return self::RETURN_CODE_NOOP_DEFAULT;
    }
}
