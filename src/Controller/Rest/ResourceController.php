<?php declare(strict_types=1);

namespace ATS\ResourceBundle\Controller\Rest;

use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use ATS\ResourceBundle\Document\Resource;
use ATS\ResourceBundle\Manager\ResourceManager;
use ATS\ResourceBundle\Service\ResourceService;
use Gedmo\Uploadable\UploadableListener;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ResourceController
 *
 * @author Ali TURKI <aturki@ats-digital.com>
 */
class ResourceController extends Controller
{

    use RestControllerTrait;

    /**
     * Upload action
     *
     * @Route(
     *     name="ats_resource_file_operation_upload",
     *     path="/resource",
     *     methods="POST"
     * )
     *
     * @param Request                  $request
     * @param ResourceService $resourceService
     *
     * @return JsonResponse
     */
    public function uploadAction(Request $request, ResourceService $resourceService)
    {
        $subFolder = $request->get('subdir');
        $uploadedFile = $request->files->get('file');

        if ($uploadedFile === null) {
            throw new BadRequestHttpException("Missing file request");
        }

        $resource = $resourceService->newResource($uploadedFile, $subFolder);
        return $this->renderResponse($resource);
    }

    /**
     * Download action
     *
     * @Route(
     *     name="ats_resource_file_operation_download",
     *     path="/resource/{id}",
     *     methods="GET"
     * )
     *
     * @param Request $request
     * @param string $id
     * @param ResourceService $resourceService
     *
     * @return BinaryFileResponse|Response
     */
    public function downloadAction(Request $request, string $id, ResourceService $resourceService)
    {
        $resource = $resourceService->getResource($id);

        if ($resource === null || $resource->isValid() === false) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $response = new BinaryFileResponse($resource->getPath());
        $response->headers->set('Content-Type', $resource->getMimeType());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $resource->getName()
        );

        return $response;
    }
}
