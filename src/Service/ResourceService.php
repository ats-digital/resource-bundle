<?php

namespace ATS\ResourceBundle\Service;

use ATS\ResourceBundle\Document\Resource;
use ATS\ResourceBundle\Manager\ResourceManager;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ResourceService
{

    /**
     * @var UploadableManager
     **/

    private $uploadableManager;

    /**
     * @var ResourceManager
     **/

    private $resourceManager;

    /**
     * @var array|null
     **/

    private $allowedSubdirectories;

    public function __construct(ResourceManager $resourceManager, UploadableManager $uploadableManager, ?array $allowedSubdirectories)
    {
        $this->resourceManager = $resourceManager;
        $this->uploadableManager = $uploadableManager;
        $this->allowedSubdirectories = $allowedSubdirectories;
    }

    public function newResource(UploadedFile $uploadedFile, ?string $uploadSubdirectory = null)
    {
        if ($uploadSubdirectory !== null) {
            if (is_array($this->allowedSubdirectories) === true && in_array($uploadSubdirectory, $this->allowedSubdirectories) === false) {
                throw new BadRequestHttpException("Invalid subdirectory : $uploadSubdirectory");
            }
            $uploadableListener =$this->uploadableManager->getUploadableListener();
            $uploadableListener
                ->setDefaultPath($uploadableListener->getDefaultPath() . '/' . $uploadSubdirectory);
        }

        $resource = new Resource();
        $resource->setOriginalFileName($uploadedFile->getClientOriginalName());
        $this->uploadableManager->markEntityToUpload($resource, $uploadedFile);
        $this->resourceManager->update($resource);

        return $resource;
    }

    public function getResource(string $id)
    {
        /**
         * @var Resource|null
         **/

        return $this->resourceManager->getOneBy(['id' => $id]);
    }
}
