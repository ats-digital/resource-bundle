<?php declare(strict_types=1);

namespace ATS\ResourceBundle\Manager;

use ATS\CoreBundle\Manager\AbstractManager;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use ATS\ResourceBundle\Document\Resource;

/**
 * Manager class for Resource entities
 *
 * @see \ATS\CoreBundle\Manager\AbstractManager
 */
class ResourceManager extends AbstractManager
{
    public function __construct(ManagerRegistry $managerRegistry, $managerName = null)
    {
        parent::__construct($managerRegistry, Resource::class, $managerName);
    }
}
