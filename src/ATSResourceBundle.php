<?php declare(strict_types=1);

namespace ATS\ResourceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ATSResourceBundle
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ATSResourceBundle extends Bundle
{
}
