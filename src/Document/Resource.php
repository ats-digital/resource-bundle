<?php

namespace ATS\ResourceBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @ODM\Document(repositoryClass="ATS\ResourceBundle\Repository\ResourceRepository")
 * @JMS\ExclusionPolicy("all")
 * @Gedmo\Uploadable(filenameGenerator="SHA1", allowOverwrite=true, appendNumber=true)
 */
class Resource
{
    /**
     * @ODM\Field(name="id", type="integer")
     * @ODM\Id
     * @JMS\Expose
     * @JMS\Type("string")
     */
    private $id;

    /**
     * @ODM\Field(name="path", type="string")
     * @JMS\Type("string")
     * @Gedmo\UploadableFilePath
     */
    private $path;

    /**
     * @ODM\Field(name="name", type="string")
     * @JMS\Type("string")
     * @JMS\Expose
     * @Gedmo\UploadableFileName
     */
    private $name;

    /**
     * @ODM\Field(name="originalFileName", type="string")
     * @JMS\Type("string")
     * @JMS\Expose
     */
    private $originalFileName;

    /**
     * @ODM\Field(name="mime_type", type="string")
     * @JMS\Type("string")
     * @JMS\Expose
     * @Gedmo\UploadableFileMimeType
     */
    private $mimeType;

    /**
     * @ODM\Field(name="size", type="float")
     * @JMS\Type("string")
     * @JMS\Expose
     * @Gedmo\UploadableFileSize
     */
    private $size;

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Get path
    * @return string
    */
    public function getPath()
    {
        return $this->path;
    }

    /**
    * Set path
    * @return $this
    */
    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
    * Get size
    * @return float
    */
    public function getSize()
    {
        return $this->size;
    }

    /**
    * Get mimeType
    * @return string
    */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
    * Get name
    * @return string
    */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * @param mixed $originalFileName
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;
    }

    /**
    * Is valid
    * @return boolean
    */
    public function isValid()
    {
        return (new Filesystem())->exists($this->getPath()) === true && $this->isEmpty() === false;
    }

    /**
    * Is empty
    * @return boolean
    */
    public function isEmpty()
    {
        return $this->getSize() === (float) 0;
    }
}
