<?php

use Doctrine\Common\Util\Debug;

if (!function_exists('s')) {






function s($var, $depth = 2)
{
$cliMode = php_sapi_name() === 'cli';
Debug::dump($var, $depth, $cliMode);
echo PHP_EOL;
}
}

if (!function_exists('sd')) {






function sd($var, $depth = 2)
{
s($var, $depth);
exit;
}
}
