<?php declare(strict_types=1);

namespace ATS\ResourceBundle\Dev\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Default Controller
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class DefaultController extends Controller
{
    use RestControllerTrait;

    /**
     * Index action
     *
     * @return JsonResponse
     */
    public function indexAction()
    {
        $data = [
            'bundle_name' => 'ATSResourceBundle',
        ];

        return $this->renderResponse($data, Response::HTTP_OK);
    }
}
