
### General
---

Developer-friendly file upload/download capabilities for Symfony-enabled web applications.

### Setup
---

1. Install the bundle in your application
```sh
$ php composer require ats/resource-bundle
```
2. Register the following parameters :
    - `upload_dir`, which will be the root upload directory
    - `allowed_upload_subdirectories` either as an array of subdirectories (e.g. `['foo','bar']` or `~` if no subdirectory restrictions are intended)

3. Register the following bundle in the AppKernel
```php
<?php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // production-enabled bundles...
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new ATS\ResourceBundle\ATSResourceBundle(),
        ];
    }
    // ...
}
```
4. Add routing definition to your `routing.yml` file :
```yaml
resource:
    resource: "@ATSResourceBundle/Controller/Rest/ResourceController.php"
    type: "annotation"
    prefix: "/api"
```

### Configuration

- Include the following YAML nodes in your configuration :

```yaml
stof_doctrine_extensions:
    uploadable:
         default_file_path: '%uploads_dir%'
    mongodb:
        default:
            uploadable: true
```

### Usage

The following endpoints are exposed :

1. File upload : `POST /api/resource`
- Parameters :
    - `file` : should be an uploaded file
    - `subdir` (optional) : provide an upload subdirectory

- Sample output :
```json
{
    "id": "5c40598b4e05a91c291641e7",
    "name": "b4d77cf414cb32ba7c161aa48333fd996a3af15f.srt",
    "mime_type": "text/plain",
    "size": "67507"
}
```

2. File download : `GET /api/resource/:id`
- Parameters :
    - Resource ID
- Output : binary content, holding the file MIME type and file name.
- Sample headers :
```
accept-ranges →bytes
cache-control →public
connection →close
content-disposition →attachment; filename="fbf824d625885a29b24a3e68fe2348c920714752.xlsx"
content-length →65481
content-type →application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
```


### FAQ
1. My upload directory is publicly accessible from my web server, how can I ensure my resources are only accessible from my API endpoint ?
One possible approach is to restrict access as web server level, as illustrated by the following nginx configuration snippet :

```nginx
location /uploads {
  deny all;
  return 404;
}
```

Notice this has no impact on how API endpoints access upload directories, as file fetching is done at filesystem level directly.